import tensorflow as tf
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
from sklearn.model_selection import train_test_split


taille_carré = 50

print('---------DATA LOADING---------')
x_train = np.load('C:/Users/clemk/OneDrive/Documents/projetZZ3/Data/x_train_Alpine.npy' )
y_train = np.load('C:/Users/clemk/OneDrive/Documents/projetZZ3/Data/y_train_Alpine.npy')
x_train_Sal = np.load('C:/Users/clemk/OneDrive/Documents/projetZZ3/Data/x_train_Salmon.npy' )
y_train_Sal = np.load('C:/Users/clemk/OneDrive/Documents/projetZZ3/Data/y_train_Salmon.npy')

#print(np.shape(x_train))
#print(np.shape(x_train_Alp))


#x_train = np.concatenate((x_train, x_train_Alp), axis=0)
#y_train = np.concatenate((y_train, y_train_Alp), axis=0)

y_train = y_train[:, 1].reshape(-1)

print(f"Vitesses {y_train}")

x_train, x_test, y_train, y_test = train_test_split(x_train, y_train, test_size=0.2, random_state=42)

print(np.shape(x_train))
print(np.shape(y_train))


print('---------NETWORK CREATION---------')

from tensorflow.keras.applications import MobileNetV3Small

from tensorflow.keras.layers import Input, Dense, Activation,Lambda,Flatten,Dropout,LSTM,TimeDistributed,SimpleRNN
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.models import Sequential

cnn = Sequential()
cnn.add(Conv2D(16,(3,3), input_shape=(taille_carré, taille_carré, 3),activation = 'relu'))
cnn.add(MaxPooling2D((2,2)))
cnn.add(Conv2D(24,(3,3),activation = 'relu'))
cnn.add(MaxPooling2D((2,2)))
cnn.add(Conv2D(48,(3,3),activation = 'relu'))


cnn.summary()

model = Sequential()
model.add(TimeDistributed(cnn, input_shape=(100, taille_carré, taille_carré, 3)))
model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2))))
model.add(TimeDistributed(Flatten()))
model.add(SimpleRNN(64))
model.add(Dense(64, activation='relu'))
model.add(Dense(32, activation='relu'))
#model.add(Dense(64, activation='relu'))
#model.add(Dense(256, activation='relu'))
model.add(Dense(1, activation='linear'))


optimizer = tf.keras.optimizers.Adam(learning_rate=0.0001)


# Compile the model with appropriate loss function for regression
model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['mae'])
model.summary()



print('---------TRAINING---------')
history = model.fit(x_train, y_train, batch_size = 9, epochs = 200)



print('---------EVALUATION---------')
loss, accuracy = model.evaluate(x_test, y_test)
print(f'Test Loss: {loss}')
print(f'Test Accuracy: {accuracy}')


# Plot MAE for training and validation
plt.plot(history.history['mae'], label='Training MAE')
plt.xlabel('Epochs')
plt.ylabel('MAE')
plt.legend()
plt.show()

print('Random test video:')
print(f'Vitesses prédites: {model.predict(x_test)}')

print(f'Vitesses réelles {y_test}')



Salmon = model.predict(x_train_Sal)
print(Salmon)
print(y_train_Sal)
model.save("C:/Users/clemk/OneDrive/Documents/projetZZ3/Models/Alpine_200.h5")

