import cv2
import os
import numpy as np

# Spécifiez le chemin du dossier contenant les frames
dossier_frames = "D:/data/SalmonRiver/Frames/Orthorectified"

# Liste pour stocker les images
frames = []


import pandas as pd


chemin_excel = "C:/Users/clemk/OneDrive/Documents/ProjetZZ3/Validation_Salmon_River.xlsx"

# Charger le fichier Excel en un DataFrame Pandas
dataframe = pd.read_excel(chemin_excel)

print(dataframe.columns)




# Taille du carré (200x200 pixels)
taille_carré = 50
x_train = np.zeros((25,100,taille_carré,taille_carré,3))
y_train = []





compteur_iterations = 0
for fichier in os.listdir(dossier_frames):
    if fichier.endswith(".jpg"):

        print(compteur_iterations)
        # Construisez le chemin complet du fichier
        chemin_image = os.path.join(dossier_frames, fichier)

        # Chargez l'image avec OpenCV
        image = cv2.imread(chemin_image)

        for i in range(0,25):
            x, y, f,s = dataframe['x'][i], dataframe['y'][i], dataframe['Freq'][i], dataframe['Scale'][i]

            # Calculez les coordonnées du carré
            x_min, x_max = int(x - taille_carré//2), int(x + taille_carré//2)
            y_min, y_max = int(y - taille_carré//2), int(y + taille_carré//2)

            print(f"{x_min} {x_max} {y_min} {y_max}")
            carre = image[y_min:y_max, x_min:x_max]


            x_train[i][compteur_iterations] = carre



        compteur_iterations+=1

        del(image)

        if compteur_iterations == 100:
            break






for i in range(0,25):
    y_train.append((i, dataframe['vel'][i]))

np.save('C:/Users/clemk/OneDrive/Documents/ProjetZZ3/Data/x_train_Salmon.npy', x_train)
np.save('C:/Users/clemk/OneDrive/Documents/ProjetZZ3/Data/y_train_Salmon.npy', y_train)

cv2.imwrite("C:/Users/clemk/OneDrive/Documents/ProjetZZ3/output_image_Salmon.jpg", x_train[3][22])

print("Taille de x_train:" + str(np.shape(x_train)))

# Enregistrez le tableau dans un fichier CSV
